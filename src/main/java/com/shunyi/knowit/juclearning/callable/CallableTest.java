package com.shunyi.knowit.juclearning.callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author Shunyi Chen
 * @create 2021-05-12 7:15
 */
public class CallableTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        new Thread(new MyRunnable()).start();

        FutureTask futureTask = new FutureTask<String>(new MyCallable());
        new Thread(futureTask,"A").start();
        new Thread(futureTask,"B").start();//从缓存取

        System.out.println(Thread.currentThread().getName()+"  "+futureTask.get());//get方法会产生阻塞，通常把它放到最后一行
        System.out.println(Thread.currentThread().getName()+"  "+futureTask.get());
    }
}

class MyRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println("call");
    }
}

class MyCallable implements Callable<String> {

    @Override
    public String call() {
        System.out.println("call");
        return "123";
    }
}