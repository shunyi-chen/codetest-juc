package com.shunyi.knowit.juclearning;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

/**
 * 分支合并-求和计算的任务
 * @author Shunyi Chen
 * @create 2021-04-26 21:17
 */
public class ForkJoinDemo extends RecursiveTask<Long> {
    private Long start;
    private Long end;
    private Long temp = 10000L;

    public ForkJoinDemo(Long start, Long end) {
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {
        if((end - start) < temp) {
            Long sum = 0L;
            for (Long i = start; i <= end; i++) {
                sum += i;
            }
            return sum;
        } else {
            //分支合并计算
            long middle = (start + end) / 2;
            ForkJoinDemo forkJoinDemo = new ForkJoinDemo(start, middle);
            forkJoinDemo.fork();
            ForkJoinDemo forkJoinDemo2 = new ForkJoinDemo(middle+1, end);
            forkJoinDemo2.fork();

            long sum = forkJoinDemo.join() + forkJoinDemo2.join();
            return sum;
        }
    }

    public static void main(String[] args) {
//        test1();
//        try {
//            test2();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        test3();
    }

    private static void test1() {
        long start = System.currentTimeMillis();
        Long sum = 0L;
        for (Long i = 1L; i <= 10_0000_0000; i++) {
            sum += i;
        }
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+" 时间： "+(end-start));
    }

    private static void test2() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();

        ForkJoinPool forkJoinPool = new ForkJoinPool();
        ForkJoinTask<Long> forkJoinTask = new ForkJoinDemo(0L, 10_0000_0000L);
//        forkJoinPool.execute(forkJoinTask);
        ForkJoinTask<Long> rs = forkJoinPool.submit(forkJoinTask);
        long sum = rs.get();
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+" 时间： "+(end-start));
    }

    private static void test3() {
        long start = System.currentTimeMillis();

        long sum = LongStream.range(0L, 10_0000_0000L).parallel().reduce(0, Long::sum);
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+" 时间： "+(end-start));
    }
}
