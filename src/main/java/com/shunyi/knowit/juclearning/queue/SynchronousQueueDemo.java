package com.shunyi.knowit.juclearning.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * 同步队列Demo
 *
 *  SynchronousQueue
 *
 *
 * @author Shunyi Chen
 * @create 2021-05-12 15:11
 */
public class SynchronousQueueDemo {

    public static void main(String[] args) {
        //同步队列,没有容量大小，存一个取一个
        BlockingQueue synchronousQueue = new SynchronousQueue<>();
        //放入
        new Thread(() -> {
            try {
                System.out.println(Thread.currentThread().getName()+" will put 1");
                synchronousQueue.put("1");

                System.out.println(Thread.currentThread().getName()+" will put 2");
                synchronousQueue.put("2");

                System.out.println(Thread.currentThread().getName()+" will put 3");
                synchronousQueue.put("3");


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "A").start();

        //拿出
        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName() +" will take "+ synchronousQueue.take());

                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName() +" will take "+ synchronousQueue.take());

                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName() +" will take "+ synchronousQueue.take());


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }, "B").start();

    }
}
