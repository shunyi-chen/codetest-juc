package com.shunyi.knowit.juclearning.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 阻塞队列Demo
 *
 * 队列特点： FIFO 先进先出
 *
 * 写入: 如果队列满了，就必须阻塞等待
 * 读取： 如果队列是空的， 就必须等待生产
 *
 * Collection:
 *  包含：
 *  List，Set, BlockingQueue
 *
 *
 * @author Shunyi Chen
 * @create 2021-04-25 10:29
 */
public class BlockingQueueDemo {



    public static void main(String[] args) {
        try {
//            test0();
//            test1();
            test2();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 会抛异常
     * @throws InterruptedException
     */
    public static void test0() throws InterruptedException {
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue(3);
        System.out.println(blockingQueue.add("a"));
        System.out.println(blockingQueue.add("b"));
        System.out.println(blockingQueue.add("c"));
        System.out.println(blockingQueue.add("d")); //抛出异常, java.lang.IllegalStateException: Queue full

        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
//        System.out.println(blockingQueue.remove()); //抛出异常 NoSuchElementException

    }

    /**
     * 不会抛异常（不会死等）
     * @throws InterruptedException
     */
    public static void test1() throws InterruptedException {
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue(3);
        System.out.println(blockingQueue.offer("a"));
        System.out.println(blockingQueue.offer("b"));
        System.out.println(blockingQueue.offer("c"));
//        System.out.println(blockingQueue.offer("d"));
        System.out.println(blockingQueue.offer("d", 1, TimeUnit.SECONDS));

        System.out.println("peek:"+blockingQueue.peek()); // peek检查队首元素

        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll(1, TimeUnit.SECONDS));
    }

    /**
     * 存取死死等待
     *
     * @throws InterruptedException
     */
    public static void test2() throws InterruptedException {
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue(3);
        blockingQueue.put("a");
        blockingQueue.put("b");
        blockingQueue.put("c");
//        blockingQueue.put("d");// 队列没有位置了，一直阻塞

        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());   //队列取不到值，一直阻塞
    }


}
