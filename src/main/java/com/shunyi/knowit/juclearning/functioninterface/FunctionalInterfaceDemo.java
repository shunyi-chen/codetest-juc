package com.shunyi.knowit.juclearning.functioninterface;

import com.shunyi.knowit.juclearning.Callback;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * 函数式接口
 *
 * 四大原生接口：
 * 包括
 *         Consumer consumer;
 *         Function function;
 *         Predicate predicate;
 *         Supplier supplier;
 *
 * @author Shunyi Chen
 * @create 2021-04-26 20:25
 */
public class FunctionalInterfaceDemo {

    public static void main(String[] args) {
//        testFunction();
//        testPredicate();
        testConsumerSupplier();
    }

    /**
     * 函数型接口
     */
    public static void testFunction() {
        Function function1 = new Function<String, String>() {
            @Override
            public String apply(String str) {
                return str;
            }
        };
        Function function2 = (str) -> {
            return str;
        };
        Function function3 = (str) ->  str;

        System.out.println(function3.apply("12"));


        Callback<String, Integer> callback = (s) -> {
            return Integer.parseInt(s);
        };
        System.out.println(callback.call("11"));

    }

    /**
     * 断定型接口，输入一个值，返回值只能是boolean
     */
    public static void testPredicate() {
        Predicate<String> predicate = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.isEmpty();
            }
        };
        Predicate<String> predicate2 = (str) -> {
            return str.isEmpty();
        };
        Predicate<String> predicate3 = (str) -> str.isEmpty();

        System.out.println(predicate3.test(null));
        System.out.println(predicate3.test("ABCDEFG"));
    }

    /**
     *  消费和供给型接口
     */
    public static void testConsumerSupplier() {
        Consumer<String> consumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };
        consumer.accept("dsdsd");

        Consumer<String> consumer2 = (str) -> {
            System.out.println("211221");
        };
        consumer2.accept("ddd");

        Consumer<String> consumer3 = System.out::println;
        consumer3.accept("Hello consumer3");


        Supplier<String> supplier = new Supplier<String>() {
            @Override
            public String get() {
                return "5555";
            }
        };

        Supplier<String> supplier2 = () -> {
            return "4444";
        };
        System.out.println(supplier2.get());
    }
}
