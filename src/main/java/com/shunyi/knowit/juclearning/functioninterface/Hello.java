package com.shunyi.knowit.juclearning.functioninterface;

import java.util.Arrays;
import java.util.List;

/**
 *
 * 小试牛刀 - @FunctionInterface
 *
 * @author Shunyi Chen
 * @create 2021-05-12 15:31
 */
@FunctionalInterface
public interface Hello {

    String msg(String info);

    static void main(String[] args) {
        //测试普通Function
        Hello hello = p -> p + "world!";
        System.out.println("test functional: "+hello.msg("Hello "));

        //测试Consumer
        List<String> list = Arrays.asList("1", "2", "3");
        list.forEach(System.out::println);
    }
}
