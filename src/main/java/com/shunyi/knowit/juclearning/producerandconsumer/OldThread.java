package com.shunyi.knowit.juclearning.producerandconsumer;

/**
 * 旧版线程-生产者消费者的实现（口诀：等待，业务，通知）
 * @author Shunyi Chen
 * @create 2021-05-11 16:57
 */
public class OldThread {
    public static void main(String[] args) {

        Data2 data2 = new Data2();

        new Thread(()-> {
            for (int i = 0; i < 10; i++) {
                data2.increment();
            }
        }, "A").start();
        new Thread(()-> {
            for (int i = 0; i < 10; i++) {
                data2.decrement();
            }
        }, "B").start();
        new Thread(()-> {
            for (int i = 0; i < 10; i++) {
                data2.increment();
            }
        }, "C").start();
        new Thread(()-> {
            for (int i = 0; i < 10; i++) {
                data2.decrement();
            }
        }, "D").start();
    }
}

class Data2{
    private int number = 0;

    public synchronized void increment() {
        while(number != 0) {
            try {
                //等待
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //业务代码
        number++;
        System.out.println(Thread.currentThread().getName()+"  "+number);
        //通知其它线程，我完了
        this.notifyAll();
    }

    public synchronized void decrement() {
        while(number == 0) {
            try {
                //等待
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //业务代码
        number--;
        System.out.println(Thread.currentThread().getName()+"  "+number);
        //通知其它线程，我完了
        this.notifyAll();
    }
}