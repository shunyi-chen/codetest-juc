package com.shunyi.knowit.juclearning;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 读写锁demo
 *
 * 独占锁（写锁） 一次只能被一个线程占用
 * 共享锁（读锁） 多个线程可以同时占有
 *
 * 读-读 可以共享
 * 读-写 不可以共享
 * 写-写 不可以共享
 *
 * 适用场景： 一个线程写，多个线程读
 *
 * @author Shunyi Chen
 * @create 2021-04-24 18:25
 */
public class ReadWriteLockDemo {

    public static void main(String[] args) {

        MyCacheWithoutLock myCache = new MyCacheWithoutLock();
//        MyCacheWithLock myCache = new MyCacheWithLock();

        for (int i = 0; i < 5; i++) {
            int t = i;
            new Thread(() -> {
                //存值
                myCache.set("key"+t, t);
            }).start();
        }
        for (int i = 0; i < 5; i++) {
            int t = i;
            new Thread(() -> {
                //取值
                myCache.get("key"+t);
            }).start();
        }
    }
}
class MyCacheWithoutLock {

    private volatile Map<String, Object> cache = new HashMap<>();

    public void set(String key, Object value) {
        System.out.println("欲写入:"+key+","+value);
        cache.put(key, value);
        System.out.println("写入:"+key+","+value+" OK");
    }

    public Object get(String key) {
        System.out.println("欲读出:"+key);
        Object val = cache.get(key);
        System.out.println("读出:"+key+"("+val+") OK");
        return val;
    }

    public void remove(String key) {
        cache.remove(key);
    }

    public void removeAll() {
        cache.clear();
    }
}

class MyCacheWithLock {

    private volatile Map<String, Object> cache = new HashMap<>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public void set(String key, Object value) {
        lock.writeLock().lock();
        try {
            System.out.println("欲写入:"+key+","+value);
            cache.put(key, value);
            System.out.println("写入:"+key+","+value+" OK");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Object get(String key) {
        lock.readLock().lock();
        Object val = null;
        try {
            System.out.println("欲读出:"+key);
            val = cache.get(key);
            System.out.println("读出:"+key+"("+val+") OK");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.readLock().unlock();
        }
        return val;
    }
}