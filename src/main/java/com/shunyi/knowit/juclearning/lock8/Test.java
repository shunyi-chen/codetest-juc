package com.shunyi.knowit.juclearning.lock8;

import java.util.concurrent.TimeUnit;

/**
 * 8种锁执行顺序
 *
 * @author Shunyi Chen
 * @create 2021-05-11 17:27
 */
public class Test {
    public static void main(String[] args) {
//        case1();
//        case2();
        case3();
    }

    private static void case1() {
        Phone phone = new Phone();
        new Thread(()-> {
            phone.sendSms();
        }, "A").start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()-> {
            phone.call();
        }, "B").start();
    }

    private static void case2() {
        Phone2 phone = new Phone2();
        new Thread(()-> {
            phone.sendSms();
        }, "A").start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()-> {
            phone.hello();
        }, "B").start();
    }

    private static void case3() {
        Phone phone1 = new Phone();
        Phone phone2 = new Phone();
        new Thread(()-> {
            phone1.sendSms();
        }, "A").start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()-> {
            phone2.call();
        }, "B").start();
    }


}



class Phone{
    public synchronized void sendSms() {
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"  Send sms");
    }

    public synchronized void call() {
        System.out.println(Thread.currentThread().getName()+"  Call");
    }
}

class Phone2{
    public synchronized void sendSms() {
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"  Send sms");
    }

    public synchronized void call() {
        System.out.println(Thread.currentThread().getName()+"  Call");
    }

    public void hello() {
        System.out.println("Hello");
    }
}
