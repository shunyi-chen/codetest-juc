package com.shunyi.knowit.juclearning.copyonwrite;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Shunyi Chen
 * @create 2021-05-12 7:03
 */
public class MapTest {

    // ConcurrentModificationException


    public static void main(String[] args) {
//        Map<String, String> map = new HashMap<>();
        ConcurrentHashMap map = new ConcurrentHashMap<>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                map.put(UUID.randomUUID().toString().substring(0, 5), "");
                System.out.println(map);
            }, String.valueOf(i)).start();
        }

    }
}
