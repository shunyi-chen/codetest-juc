package com.shunyi.knowit.juclearning.copyonwrite;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author Shunyi Chen
 * @create 2021-05-12 7:03
 */
public class SetTest {

    public static void main(String[] args) {
        Set<String> set0 = new HashSet<>();
//        Set<String> set = Collections.synchronizedSet(set0);
        Set<String> set = new CopyOnWriteArraySet<>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                set.add(UUID.randomUUID().toString().substring(0, 5));
                System.out.println(set);
            }, String.valueOf(i)).start();
        }

    }
}
