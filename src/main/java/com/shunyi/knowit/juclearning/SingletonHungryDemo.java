package com.shunyi.knowit.juclearning;

/**
 * 单例饿汉式
 * @author Shunyi Chen
 * @create 2021-04-28 11:26
 */
public class SingletonHungryDemo {

    private byte[] data1 = new byte[1024*1024];
    private byte[] data2 = new byte[1024*1024];
    private byte[] data3 = new byte[1024*1024];
    private byte[] data4 = new byte[1024*1024];

    private final static SingletonHungryDemo OBJ = new SingletonHungryDemo();

    private SingletonHungryDemo() {
        System.out.println(Thread.currentThread().getName()+" ok");
    }

    private static SingletonHungryDemo getInstance() {
        return OBJ;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            new Thread(()-> {
                SingletonLazyDemo.getInstance();
            }).start();
        }
    }
}
