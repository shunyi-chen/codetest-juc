package com.shunyi.knowit.juclearning;

import java.util.concurrent.TimeUnit;

/**
 * 死锁Demo
 *
 * 查看死锁命令:
 *  查看进程号: jps -l
 *  查看堆栈: jstack 12020
 *
 *
 *
 * @author Shunyi Chen
 * @create 2021-04-29 14:34
 */
public class DeadLockDemo {

    public static void main(String[] args) {
          String lockA = "lockA";
          String lockB = "lockB";

        new Thread(new MyThread(lockA, lockB), "T1").start();
        new Thread(new MyThread(lockB, lockA), "T2").start();
    }
}

/**
 *
 */
class MyThread implements Runnable {

    private String lockA;
    private String lockB;

    public MyThread(String lockA, String lockB) {
        this.lockA = lockA;
        this.lockB = lockB;
    }

    @Override
    public void run() {

        synchronized (lockA) {
            System.out.println(Thread.currentThread().getName()+" lock "+lockA+" get "+lockB);

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (lockB) {
                System.out.println(Thread.currentThread().getName()+" lock "+lockB+" get "+lockA);
            }
        }

    }
}