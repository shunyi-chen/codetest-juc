package com.shunyi.knowit.juclearning.stream;

import com.shunyi.knowit.juclearning.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 流式计算/链式编程
 *
 *  map与flatmap区别 https://www.cnblogs.com/yucy/p/10260014.html
 *
 * @author Shunyi Chen
 * @create 2021-04-26 21:04
 */
public class StreamDemo {
    // 自增生成组编号
    static int group = 1;
    // 自增生成学生编号
    static int student = 1;


    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        User user1 = new User(1, "a", 21);
        User user2 = new User(2, "b", 22);
        User user3 = new User(3, "c", 23);
        User user4 = new User(4, "d", 24);
        User user5 = new User(6, "e", 25);
        List<User> lst = Arrays.asList(user1, user2, user3, user4, user5);

        lst.stream()
                .filter(u -> u.getId()%2==0)
                .filter(u -> u.getAge() > 23)
                .map(u -> u.getName().toUpperCase())
                .sorted((u1, u2)->{return u2.compareTo(u1);})
                .limit(10)
                .forEach(System.out::println);


        //map与flatMap区别
        List<String[]> eggs = new ArrayList<>();
        // 第一箱鸡蛋
        eggs.add(new String[]{"鸡蛋_1", "鸡蛋_1", "鸡蛋_1", "鸡蛋_1", "鸡蛋_1"});
        // 第二箱鸡蛋
        eggs.add(new String[]{"鸡蛋_2", "鸡蛋_2", "鸡蛋_2", "鸡蛋_2", "鸡蛋_2"});

        eggs.stream().map(x -> Arrays.stream(x).map(y -> y.replace("鸡", "煎")))
            .forEach(x -> System.out.println("组" + group++ + ":" + Arrays.toString(x.toArray())));

        eggs.stream().flatMap(x -> Arrays.stream(x).map(y -> y.replace("鸡", "煎")))
            .forEach(x -> System.out.println("学生" + student++ + ":" + x));

    }
}
