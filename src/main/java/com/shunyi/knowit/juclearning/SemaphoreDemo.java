package com.shunyi.knowit.juclearning;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * 抢车位程序
 *
 * @author Shunyi Chen
 * @create 2021-04-24 9:51
 */
public class SemaphoreDemo {

    public static void main(String[] args) {
        //有5个车位
        Semaphore semaphore = new Semaphore(5);
        //有10辆车去抢
        for (int i = 0; i < 10; i++) {
            final int n = i;
            new Thread(() -> {
                try {
                    semaphore.acquire();

                    System.out.println("==========>车"+n+"抢到车位");
                    TimeUnit.SECONDS.sleep(5);
                    System.out.println("车"+n+"离开车位");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                }
            }, "线程"+n).start();
        }

    }
}
