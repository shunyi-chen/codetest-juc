package com.shunyi.knowit.juclearning;

/**
 * @author Shunyi Chen
 * @create 2021-04-18 21:01
 */
public class SaleTicketDemo01{

    public static void main(String[] args) {
        System.out.println("Running SaleTicketDemo01");
        Ticket ticket = new Ticket();

        new Thread(()-> {
            for (int i = 0; i < 40; i++) {
                ticket.sale();
            }
        },"name01").start();
        new Thread(()->{
            for (int i = 0; i < 40; i++) {
                ticket.sale();
            }
        },"name02").start();
        new Thread(()->{
            for (int i = 0; i < 40; i++) {
                ticket.sale();
            }
        }, "name3").start();
    }

}
class Ticket {

    private int number = 30;

    public synchronized void sale() {
        if(number > 0) {
            System.out.println(Thread.currentThread().getName()+" 卖出了 "+(number--)+"张票，剩余"+number);
        }
    }

}