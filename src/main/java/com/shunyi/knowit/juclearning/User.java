package com.shunyi.knowit.juclearning;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Shunyi Chen
 * @create 2021-04-26 21:05
 */

@AllArgsConstructor
@Data
public class User {
    private int id;
    private String name;
    private int age;
}
