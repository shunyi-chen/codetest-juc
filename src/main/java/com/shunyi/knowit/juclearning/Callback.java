package com.shunyi.knowit.juclearning;

public interface Callback<P,R> {

    R call(P p);
}
