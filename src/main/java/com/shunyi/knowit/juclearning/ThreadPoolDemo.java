package com.shunyi.knowit.juclearning;

import java.util.concurrent.*;

/**
 * 线程池及7参数Demo
 * @author Shunyi Chen
 * @create 2021-04-25 11:31
 */
public class ThreadPoolDemo {

    public static void main(String[] args) {

        System.out.println(Runtime.getRuntime().availableProcessors());


//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        ExecutorService executorService = Executors.newFixedThreadPool(5);
//        ExecutorService executorService = Executors.newCachedThreadPool();  //遇强则强
//        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(5);
//        for (int i = 0; i < 10; i++) {
//            executorService.schedule(() -> {
//                System.out.println(Thread.currentThread().getName() + " ok.");
//            }, 5, TimeUnit.SECONDS);
//        }
//        executorService.shutdown();

         //用原生方式创建pool
         ExecutorService threadPool = new ThreadPoolExecutor(
                 2,
                 5, ///设置两种方式CPU密集型（Runtime.getRuntime().availableProcessors()），IO密集型（查看有多少个IO耗时的线程的两倍）
                 3,
                 TimeUnit.SECONDS,
                 new LinkedBlockingDeque<>(1),
                 Executors.defaultThreadFactory(),
//                 new ThreadPoolExecutor.AbortPolicy()//银行人满了，如果人进来了，不处理这个人，抛出异常
//                 new ThreadPoolExecutor.DiscardOldestPolicy()
//                 new ThreadPoolExecutor.DiscardPolicy()
                 new ThreadPoolExecutor.CallerRunsPolicy()
            );
        for (int i = 1; i <= 19; i++) {
            threadPool.execute(() -> {
                System.out.println(Thread.currentThread().getName()+" ok");
            });
        }
        threadPool.shutdown();
    }
}
