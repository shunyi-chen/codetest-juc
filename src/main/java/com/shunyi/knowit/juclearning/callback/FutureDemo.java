package com.shunyi.knowit.juclearning.callback;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 异步回调
 * @author Shunyi Chen
 * @create 2021-04-27 10:04
 */
public class FutureDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        //没有返回值的异步调用
        CompletableFuture<Void> comparableFuture = CompletableFuture.runAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"  run async => void ");

        });

        System.out.println("11111");
        comparableFuture.get();//会阻塞主线程

        ///有返回值的异步调用
        CompletableFuture<Integer> completableFuture2 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int s = 10 / 0;
            return 123;
        });

        int a = completableFuture2.whenComplete((t, u) -> {
            System.out.println("t=>"+t);//正常返回结果
            System.out.println("u=>"+u);
        }).exceptionally((e) -> {
            System.out.println(e.getMessage());
            return 444;
        }).get();
        System.out.println("a="+a);

    }
}
