package com.shunyi.knowit.juclearning.jmm;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Java内存模型
 * 包含volatile, 原子类 AtomicInteger
 *
 *
 * @author Shunyi Chen
 * @create 2021-04-28 7:17
 */
public class JMMDemo {

    /*
        volatile关键字作用:
        1.保证共享变量可见性
        2.不保证原子性
        3.禁止（避免）指令重排， 解决：增加内存屏障保证指令执行顺序



     */

    public static void main(String[] args) {
//        test1();
//        test2();
        test3();
    }

    private volatile static int NUM = 0; // 不加volatile程序会死循环，加上则能保证可见性

    /**
     * 验证volatile保证可见性测试
     */
    public static void test1() {
        new Thread(() -> {
            while(NUM == 0) {
            }
        }).start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        NUM = 1;
        System.out.println(NUM);
    }



    private volatile static int num = 0;
    private static void add() {
        num++;
    }

    /**
     * 验证volatile不保证原子性(这里假设不想用lock和synchronized解决，用CAS解决)
     */
    public static void test2() {
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    add();
                }
            }).start();
        }

        //大于2是因为java默认存在两个线程 main和gc
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }

        System.out.println(Thread.currentThread().getName()+"  "+num);
    }


    private volatile static AtomicInteger num3 = new AtomicInteger();
    private static void add3() {
        num3.getAndIncrement(); // Atomic integer + 1 方法， CAS
    }

    /**
     * 验证不使用lock和sychrpmized去如何保证原子性
     */
    public static void test3() {
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    add3();
                }
            }).start();
        }

        //大于2是因为java默认存在两个线程 main和gc
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }

        System.out.println(Thread.currentThread().getName()+"  "+num3);
    }
}
