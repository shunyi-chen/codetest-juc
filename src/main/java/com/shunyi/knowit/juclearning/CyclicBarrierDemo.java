package com.shunyi.knowit.juclearning;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * 集齐7个龙珠召唤神龙 --加法计数器
 * @author Shunyi Chen
 * @create 2021-04-24 10:01
 */
public class CyclicBarrierDemo {

    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7, () -> {
            System.out.println("===========>召唤神龙成功！");
        });

        for (int i = 0; i < 14; i++) {
            final int n = i;
            new Thread(() -> {
                System.out.println("集齐第"+n+"号龙珠");


                try {
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }

            }).start();
        }
    }
}
