package com.shunyi.knowit.juclearning;

import java.util.concurrent.CountDownLatch;

/**
 * -减法计数器
 * @author Shunyi Chen
 * @create 2021-05-12 7:56
 */
public class CountDownLatchDemo {

    public static void main(String[] args) {

        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i = 0; i < 6; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName()+" Go out");
                countDownLatch.countDown();
            }, "Thread-"+i+"").start();
        }
        try {
            countDownLatch.await();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("done");
    }
}
