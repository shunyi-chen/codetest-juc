package com.shunyi.knowit.juclearning.cas;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @author Shunyi Chen
 * @create 2021-05-13 14:17
 */
public class ABAProblem {

    public static void main(String[] args) {
//        test1();
        test2();
    }

    /**
     * ABA问题
     */
    public static void test1() {
        AtomicInteger atomicInteger = new AtomicInteger(2020);

        //捣乱的线程
        System.out.println(atomicInteger.compareAndSet(2020, 2021));
        System.out.println(atomicInteger.get());

        System.out.println(atomicInteger.compareAndSet(2021, 2020));
        System.out.println(atomicInteger.get());

        //期望的线程
        System.out.println(atomicInteger.compareAndSet(2020, 2022));
        System.out.println(atomicInteger.get());
    }

    /**
     * 解决ABA问题
     */
    public static void test2() {
        AtomicStampedReference atomicInteger = new AtomicStampedReference(2, 1);

        new Thread(() -> {
            int stamp = atomicInteger.getStamp();//获取版本号
            System.out.println("a1=>"+stamp);

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            atomicInteger.compareAndSet(2, 4, atomicInteger.getStamp(), atomicInteger.getStamp() + 1);
            System.out.println("a2=>"+atomicInteger.getStamp());

            atomicInteger.compareAndSet(4, 2, atomicInteger.getStamp(), atomicInteger.getStamp() + 1);
            System.out.println("a3=>"+atomicInteger.getStamp());

        }, "A").start();

        new Thread(() -> {
            int stamp = atomicInteger.getStamp();//获取版本号
            System.out.println("b1=>"+stamp);

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            System.out.println(atomicInteger.compareAndSet(2, 6, atomicInteger.getStamp(), atomicInteger.getStamp() + 1));
            System.out.println("b2=>"+atomicInteger.getStamp());

        }, "B").start();
    }

}
