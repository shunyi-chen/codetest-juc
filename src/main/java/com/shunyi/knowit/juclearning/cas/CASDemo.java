package com.shunyi.knowit.juclearning.cas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * CAS展示
 *
 * CAS: 比较当前工作内存中的值和主内存中的值，如果这个值是期望的，那么则执行操作，如果不是就一直循环
 * 缺点：
 *   循环会耗时
 *   一次只能保证一个共享变量的原子性
 *   ABA问题
 *
 * @author Shunyi Chen
 * @create 2021-04-29 15:29
 */
public class CASDemo {

    public static void main(String[] args) {
        AtomicInteger ai = new AtomicInteger(20210429);

        //如果期望值达到了则更新，否则不更新
        boolean b1 = ai.compareAndSet(20210429, 20210430);
        System.out.println(b1);
        int i = ai.getAndIncrement();
        System.out.println(ai.get());

        boolean b2 = ai.compareAndSet(20210429, 20210501);
        System.out.println(b2);
        System.out.println(ai.get());


    }

}
