package com.shunyi.knowit.juclearning;

/**
 * 单例懒汉模式
 *
 * Java反编译命令
 * 1 javap -p .\BlockingQueueDemo.class
 * 2 .\jad.exe -sjava .\BlockingQueueDemo.class
 *
 *
 * @author Shunyi Chen
 * @create 2021-04-28 11:29
 */
public class SingletonLazyDemo {

    private SingletonLazyDemo() {
        System.out.println(Thread.currentThread().getName()+" ok");
        synchronized (SingletonLazyDemo.class) {
            if(OBJ != null) {
                throw new RuntimeException("不要试图用反射破坏异常");
            }
        }
    }

    private volatile static SingletonLazyDemo OBJ;

    public static SingletonLazyDemo getInstance() {
        if(OBJ == null) {
            synchronized (SingletonLazyDemo.class) {
                if(OBJ == null) {
                    OBJ = new SingletonLazyDemo(); //不是原子性操作
                    /*
                        1.分配内存空间
                        2.执行构造方法，初始化对象
                        3.把这个对象指向这个空间
                     */
                }
            }
        }
        return OBJ;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            new Thread(()-> {
                SingletonLazyDemo.getInstance();
            }).start();
        }
    }
}
