package com.shunyi.knowit.juclearning;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Shunyi Chen
 * @create 2021-04-18 21:01
 */
public class SaleTicketDemo02 {

    public static void main(String[] args) {
        System.out.println("Running SaleTicketDemo02");
        Ticket2 ticket = new Ticket2();

        new Thread(()-> {for (int i = 0; i < 40; i++) ticket.sale();},"name01").start();
        new Thread(()-> {for (int i = 0; i < 40; i++) ticket.sale();},"name02").start();
        new Thread(()-> {for (int i = 0; i < 40; i++) ticket.sale();},"name3").start();
    }

}

class Ticket2 {

    private int number = 130;

    private Lock lock = new ReentrantLock();

    public void sale() {
        lock.lock();
        try {
            if(number > 0) {
                System.out.println(Thread.currentThread().getName()+" 卖出了 "+(number--)+"张票，剩余"+number);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

}